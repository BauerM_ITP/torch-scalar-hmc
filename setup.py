from setuptools import setup, find_packages

setup(
    name='torch_hmc',
    version="1.0",
    packages=find_packages(),
    install_requires=[
        'torch',
    ],
    author='Marc Bauer',
)
    