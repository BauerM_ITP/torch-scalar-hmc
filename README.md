# Scalar $\varphi^4$-theory on the lattice using Pytorch

A simple Pytorch implementation of scalar $\varphi^4$-theory on a hypercubic lattice in arbitrary dimensions using Hybrid-Monte-Carlo. We use the action convention

$$
S = \sum_{x} \left( \frac{1}{2} \sum_{\mu} \left( \varphi_{x+\mu} - \varphi_{x} \right)^2 + \frac{1}{2} m^2 \varphi_{x}^2 + \frac{\lambda}{4} \varphi_{x}^4 \right)\,,
$$
where $\mu$ points to the $2d$ nearest neighbors of $x$ on a hypercubic lattice.

The fields are treated in a batched manner, and functions are partially compiled using Pytorch 2 compilation. This leads to relatively fast simulations on GPUs, even with a simple implementation.

![config](plots/field_configurations.png)

## Dependencies
The main code only requires `torch`, and was tested with version 1.3.1.
The accompanying Jupyter notebook `examples.ipynb` also requires `matplotlib`, `tqdm`, `pandas` and `numpy`.

An environment file is provided in `environment.yml` for easy installation of the dependencies using conda:

```bash
conda env create -f environment.yml
conda activate mcmc_env
```

## Installation

To use the module in other projects, simple clone the repository and install using pip from the root directory:

```bash
pip install .
```

## Usage & Examples
The usage is exemplified in the Jupyter notebook `examples.ipynb` on a 2D lattice.
Currently, only a $\varphi^4$ action is supported, but other action can be added by extending `mcmc_action.py` based on the template provided by the class `Phi4Action`.

The inner loops of the leapfrog-integrator are compiled via Pytorch's `@torch.compile`. The depth of the compilations has to be set manually in order to balance performance, as unrolling the loops can lead to a significant increase in compilation time.

In 2D, the Z2-symmetry of the system is spontaneously broken at some critical value of the coupling constant $\lambda$.  Finding the critical value exactly is a bit tricky, as the system is finite and a continuum extrapolation has to be taken. A rough estimate of the position can be obtained by plotting the magnetization as a function of the coupling, with various lattice sizes. We find a sharpening onset of the magentization as the lattice size increases, around $\lambda = 4.25$ with $m^2 = -4$.

<div align="center">
![vol_scan](plots/phi4_vol_scan.png){width=70%}
</div>

At fixed volume, we can also scan the mass, from which we can compute the magnetization phase diagram. Below we have computed the at a lattice volume of $V = 64^2$ using as set of $\sim 400$ data points and bicubic interpolation.

<div align="center">
![phase_diag](plots/phi4_phase_diag_64.png){width=70%}
</div>







