"""
Actions

Actions can be added by simply following the template provided by Phi4Action.
"""

import torch


class Phi4Action:
    """
    Implements the Phi^4 Action with parameter conventions m²/2 * phi² + g/4 * phi⁴
    Can compute both the action density and the drift.

    members:
        m2 (float): The mass squared parameter.
        g (float): The coupling parameter.
        
    methods:
        __call__(cfgs: torch.tensor): Calculates the action density for a given set of
        configurations.
        drift(cfgs: torch.tensor): Calculates the drift for a given set of configurations.

    """

    def __init__(self, m2 : float, g: float):
        self.m2 = m2
        self.g = g

    @torch.compile
    def __call__(self, cfgs: torch.tensor):
        """
        Calculates the action density for a given set of configurations.

        args:
            cfgs (torch.tensor): The input configurations.

        returns:
            torch.tensor: The action density.

        """
        action_density = self.m2 * cfgs**2 + self.g / 2 * cfgs**4

        num_dims = len(cfgs.shape) - 1
        dims = range(1, num_dims + 1)

        for mu in dims:
            action_density += 2 * cfgs**2
            action_density -= cfgs * torch.roll(cfgs, -1, mu)
            action_density -= cfgs * torch.roll(cfgs, 1, mu)

        return torch.sum(action_density / 2, dim=tuple(dims))

    @torch.compile
    def drift(self, cfgs):
        """
        Calculates the drift for a given set of configurations.

        args:
            cfgs (torch.tensor): The input configurations.

        returns:
            torch.tensor: The drift.

        """
        drift = 2 * self.m2 * cfgs + 4 * self.g / 2 * cfgs**3

        num_dims = len(cfgs.shape) - 1
        dims = range(1, num_dims + 1)

        for mu in dims:
            drift += 4 * cfgs
            drift -= 2 * torch.roll(cfgs, -1, mu)
            drift -= 2 * torch.roll(cfgs, 1, mu)

        drift /= 2

        return -drift
