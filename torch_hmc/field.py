"""Field class for the MCMC sampler."""

import torch
from torch_hmc.mcmc_action import Phi4Action


class BatchedField:
    """Batched field class, holding multiple fields.

    This class represents a batch of fields, where each field is a multi-dimensional lattice.

    Attributes:
        lat_len (int): The length of each dimension of the lattice.
        dim (int): The number of dimensions of the lattice.
        batch_size (int): The number of fields in the batch.
        phi (torch.tensor): The field values.
        phiold (torch.tensor): The previous field values.

    Methods:
        set_constant(value: float): Sets the field to a constant value.

        accept_reject(mask: torch.tensor): Accepts or rejects configurations based on the
            mask.

        inner_loop(n_steps: int, dt: float, chi: torch.tensor, action: Phi4Action): Computes
            some iterations of the HMC body loop.

        hmc_step(n_steps: int, dt: float, action: Phi4Action): Makes a single step in the HMC  Markov chain.

        random_flip(p: float = 0.5): Randomly flips all fields (per sample) with
            probability p.

        compute_magnetization(): Computes the magnetization of the field.


    """

    def __init__(self, dim: int, lat_len: int, batch_size: int, device: str = "cpu"):
        """
        Initializes a new instance of the BatchedField class.

        Args:
            dim (int): The number of dimensions of the lattice.
            lat_len (int): The length of each dimension of the lattice.
            batch_size (int): The number of fields in the batch.
            device (str, optional): The device to store the fields on (e.g., "cpu" or "cuda").
                Defaults to "cpu".
        """
        self.lat_len = lat_len
        self.dim = dim
        self.batch_size = batch_size

        self.phi = (torch.randn(batch_size, *[lat_len] * dim) * 0.1).to(device)
        self.phi.requires_grad = False
        self.phiold = self.phi.clone()

    def set_constant(self, value: float):
        """Sets the field to a constant value.

        Args:
            value (float): The value to set the field to.
        """
        self.phi.fill_(value)
        self.phiold.fill_(value)

    @torch.compile
    def accept_reject(self, mask: torch.tensor):
        """Accepts or rejects configurations based on the mask.

        Args:
            mask (torch.tensor): A boolean mask indicating which configurations to accept or
                reject.
        """
        self.phi[mask] = self.phiold[mask]

    # compiled sub-loop, torch doesn't like unrolling the whole thing
    @torch.compile
    def inner_loop(self, n_steps: int, dt: float, chi: torch.FloatTensor, action: Phi4Action):
        """Computes some iterations of the HMC (Hamiltonian Monte Carlo) body loop.

        args:
            n_steps (int): The number of iterations to compute.
            dt (float): The time step size.
            chi (torch.tensor): The momentum variable.
            action (Phi4Action): The action function.
        """
        for _ in torch.arange(n_steps):
            self.phi += dt * chi
            chi += dt * action.drift(self.phi)

    def hmc_step(
        self, n_steps: int, dt: float, action: Phi4Action, compile_steps: int = 10
    ) -> torch.BoolTensor:
        """Makes a single step in the HMC (Hamiltonian Monte Carlo) Markov chain.

        args:
            n_steps (int): The number of iterations to perform in the HMC loop.
            dt (float): The time step size.
            action (Phi4Action): The action function.

        returns:
            torch.tensor: A boolean tensor indicating whether each configuration was accepted
                or rejected.
        """
        self.phiold = self.phi.clone()
        action_old = action(self.phi)

        chi = torch.randn_like(self.phi, device=self.phi.device)
        h_old = action_old + 0.5 * torch.sum(chi**2, dim=tuple(range(1, self.dim + 1)))

        chi += 0.5 * dt * action.drift(self.phi)

        inner_loop_steps = compile_steps
        outer_loop = torch.arange(int(n_steps / inner_loop_steps))
        rest_loop_steps = n_steps % inner_loop_steps

        for _ in outer_loop:
            self.inner_loop(inner_loop_steps, dt, chi, action)

        self.inner_loop(rest_loop_steps, dt, chi, action)

        chi -= 0.5 * dt * action.drift(self.phi)

        # If necessary, this can be changed to simply reject runaway configurations.
        if not torch.all(torch.isfinite(self.phi)):
            raise ValueError("Runaway detected in HMC. Infinite values in field.")

        action_new = action(self.phi)
        d_h = action_new + 0.5 * torch.sum(chi**2, dim=tuple(range(1, self.dim + 1))) - h_old

        mask = torch.greater(d_h, 0)

        mask = mask & torch.greater_equal(
            torch.rand(mask.shape, device=mask.device), torch.exp(-d_h)
        )

        self.accept_reject(mask)
        return ~mask

    def random_flip(self, p: float = 0.5):
        """
        Randomly flips all fields (per sample) with probability p.

        args:
            p (float): The probability of flipping each field.
        """
        mask = torch.rand(self.batch_size, device=self.phi.device) < p
        self.phi[mask] = -self.phi[mask]

    @torch.compile
    def compute_magnetization(self) -> torch.FloatTensor:
        """Computes the magnetization of the field.

        Returns:
            torch.tensor: The magnetization of the field.
        """
        return torch.mean(self.phi, dim=tuple(range(1, self.dim + 1)))

    @torch.compile
    def compute_correlation(self):
        """
        Computes the full correlation for the field using FFT.

        Returns:
            correlation (torch.tensor) : (batch_size, *(lattice_shape))
                The correlation of the field.
        """
        # pylint: disable=not-callable
        phi_mom = torch.fft.rfftn(self.phi, dim=tuple(range(1, self.dim + 1)))
        correlation = torch.fft.irfftn(
            phi_mom * torch.conj(phi_mom), dim=tuple(range(1, self.dim + 1))
        )
        # pylint: enable=not-callable

        return correlation / self.lat_len**self.dim
